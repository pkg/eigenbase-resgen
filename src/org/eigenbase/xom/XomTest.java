/*
// $Id: //open/util/resgen/src/org/eigenbase/xom/XomTest.java#1 $
// Package org.eigenbase.xom is an XML Object Mapper.
// Copyright (C) 2008-2008 The Eigenbase Project
// Copyright (C) 2008-2008 Disruptive Tech
// Copyright (C) 2008-2008 LucidEra, Inc.
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version approved by The Eigenbase Project.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package org.eigenbase.xom;

import junit.framework.TestCase;

/**
 * Unit tests for XOM module.
 *
 * @author jhyde
 * @version $Id: //open/util/resgen/src/org/eigenbase/xom/XomTest.java#1 $
 * @since Jun 6, 2008
 */
public class XomTest extends TestCase {
    public void testFoo() throws XOMException {
        final Parser xmlParser = XOMUtil.createDefaultParser();
        xmlParser.setKeepPositions(true);
        final String lineSep = System.getProperty("line.separator");
        final String xml = "<Model" + lineSep
            + "  name=\"meta\"" + lineSep
            + "  dtdName=\"meta.dtd\"" + lineSep
            + "  className=\"MetaDef\"" + lineSep
            + "  packageName=\"org.eigenbase.xom\"" + lineSep
            + "  root=\"Model\"" + lineSep
            + "  version=\"1.0\"" + lineSep
            + ">" + lineSep
            + "  <!-- a comment" + lineSep
            + "       spread across multiple lines -->" + lineSep
            + "<Doc>" + lineSep
            + "  This model is the XOM Meta Model.  It is the specification of the model used" + lineSep
            + "  to define new XML-based models.  It is also an instance of itself." + lineSep
            + "" + lineSep
            + "</Doc>" + lineSep
            + "" + lineSep
            + "<Element type=\"Model\">" + lineSep
            + "    <Doc>" + lineSep
            + "       Contains a \"single\" apostrope '." + lineSep
            + "    </Doc>" + lineSep
            + "" + lineSep
            + "    <Attribute name=\"name\" required=\"true\"/>" + lineSep
            + "    <Attribute name=\"dtdName\"/>" + lineSep
            + "</Element>" + lineSep
            + "</Model>";
        DOMWrapper def = xmlParser.parse(xml);
        assertNotNull(def);
        final MetaDef.Model model = new MetaDef.Model(def);
        assertNotNull(model);

        Location location = model.getLocation();
        assertEquals("Model", model.getName());
        assertEquals(1, location.getStartLine());
        assertEquals(1, location.getStartColumn());
        assertEquals(25, location.getEndLine());
        assertEquals(9, location.getEndColumn());

        // Model only has one child, Element. Doc is Cdata, so becomes an
        // attribute.
        NodeDef[] children = model.getChildren();
        assertEquals(1, children.length);
        final NodeDef element = children[0];
        assertEquals("Element", element.getName());
        location = element.getLocation();
        assertEquals(17, location.getStartLine());
        assertEquals(1, location.getStartColumn());
        assertEquals(24, location.getEndLine());
        assertEquals(11, location.getEndColumn());

        children = element.getChildren();
        assertEquals(4, children.length);
        NodeDef attribute = children[1];
        assertEquals("Attribute", attribute.getName());
        location = attribute.getLocation();
        assertEquals(23, location.getStartLine());
        assertEquals(5, location.getStartColumn());
        assertEquals(23, location.getEndLine());
        assertEquals(32, location.getEndColumn());
    }
}

// End XomTest.java
