/*
// $Id: //open/util/resgen/src/org/eigenbase/xom/Locator.java#1 $
// Package org.eigenbase.xom is an XML Object Mapper.
// Copyright (C) 2008-2008 The Eigenbase Project
// Copyright (C) 2008-2008 Disruptive Tech
// Copyright (C) 2008-2008 LucidEra, Inc.
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version approved by The Eigenbase Project.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package org.eigenbase.xom;

/**
 * Callback to find the location of a node within its document.
 *
 * @author jhyde
 * @version $Id: //open/util/resgen/src/org/eigenbase/xom/Locator.java#1 $
 * @since Jun 6, 2008
 */
public interface Locator {
    Location getLocation(DOMWrapper wrapper);
}

// End Locator.java
