/*
// $Id: //open/util/resgen/src/org/eigenbase/xom/Location.java#2 $
// Package org.eigenbase.xom is an XML Object Mapper.
// Copyright (C) 2008-2008 The Eigenbase Project
// Copyright (C) 2008-2008 Disruptive Tech
// Copyright (C) 2008-2008 LucidEra, Inc.
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version approved by The Eigenbase Project.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package org.eigenbase.xom;

/**
 * Represents the location of a node within its document.
 *
 * <p>Location is a span from a starting line and column to an ending line
 * and column; or alternatively, from a starting character position to an
 * ending character position.
 *
 * @author jhyde
 * @version $Id: //open/util/resgen/src/org/eigenbase/xom/Location.java#2 $
 * @since Jun 6, 2008
 */
public interface Location {
    /**
     * Returns the line where this node starts.
     * The first line in the document is 1.
     *
     * @return Line of the start of this node
     */
    int getStartLine();

    /**
     * Returns the column where this node starts.
     * The first column in the document is 1.
     *
     * @return column of the start of this node
     */
    int getStartColumn();

    /**
     * Returns the character position where this node starts.
     * The first character in the document is 0.
     *
     * @return Character position of the start of this node
     */
    int getStartPos();

    /**
     * Returns the line where this node ends.
     * The first line in the document is 1.
     *
     * @return Line of the end of this node
     */
    int getEndLine();

    /**
     * Returns the column where this node ends.
     * The first column in the document is 1.
     *
     * @return column of the end of this node
     */
    int getEndColumn();

    /**
     * Returns the character position where this node ends.
     * The first character in the document is 0.
     *
     * @return Character position of the end of this node
     */
    int getEndPos();

    /**
     * Returns the text of this location.
     *
     * <p>If this location is an element
     * and <code>headOnly</code> is true, returns only the text of the head
     * of the element. For example,
     *
     * <blockquote><pre>
     * &lt;Foo a="1" b="2"&gt;
     *   &lt;Bar c="3"&gt;
     * &lt;/Foo&gt;
     * </pre></blockquote>
     *
     * returns "&lt;Foo a='1' b='2'&gt;&lt;Bar c='3'&gt;&lt;/Foo&gt;"
     * if <code>headOnly</code> is false, "&lt;Foo a='1' b='2'&gt;" if it is
     * true.
     *
     * @param headOnly Whether to return only the head of elements
     * @return Source text underlying a location
     */
    String getText(boolean headOnly);
}

// End Location.java
