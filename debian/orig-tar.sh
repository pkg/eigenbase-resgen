#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=eigenbase-resgen-$2
TAR=eigenbase-resgen_$2.orig.tar.gz

# Repack upstream source to tar.gz
# - strip generated API docs
unzip $3
mv resgen $DIR
GZIP=--best tar czf $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR
